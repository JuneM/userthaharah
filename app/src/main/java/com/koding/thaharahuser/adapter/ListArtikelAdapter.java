package com.koding.thaharahuser.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.koding.thaharahuser.R;
import com.koding.thaharahuser.model.ModelArtikel;
import com.koding.thaharahuser.ui.DetailArtikelActivity;

import java.util.ArrayList;

public class ListArtikelAdapter extends RecyclerView.Adapter<ListArtikelAdapter.ListArtikelViewHolder> {

    private Context context;
    private ArrayList<ModelArtikel> listArtikel;
    public ListArtikelAdapter(Context context, ArrayList<ModelArtikel> list) {
        this.context = context;
        this.listArtikel = list;
    }

    @NonNull
    @Override
    public ListArtikelViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_row_hero, viewGroup, false);
        return new ListArtikelViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ListArtikelViewHolder holder, int position) {
        final ModelArtikel modelArtikel = listArtikel.get(position);
        holder.imgPhoto.setText(String.valueOf(position + 1));
        holder.tvJudulArtikel.setText(modelArtikel.getJudulArtikel());
        String isiArtikel = (String) modelArtikel.getIsiArtikel();
        if (isiArtikel.length() > 35){
            holder.tvIsiArtikel.setText(isiArtikel.substring(0,30) + "...");
        }else{
            holder.tvIsiArtikel.setText(isiArtikel);
        }
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intentDetail = new Intent(context, DetailArtikelActivity.class);
                intentDetail.putExtra(DetailArtikelActivity.EXTRA_DATA, modelArtikel);
                context.startActivity(intentDetail);
                ((Activity) context).finish();
            }
        });
    }

    @Override
    public int getItemCount() {
        return listArtikel.size();
    }

    public class ListArtikelViewHolder extends RecyclerView.ViewHolder {
        TextView imgPhoto;
        TextView tvJudulArtikel, tvIsiArtikel;

        public ListArtikelViewHolder(@NonNull View itemView) {
            super(itemView);
            imgPhoto = itemView.findViewById(R.id.img_item_photo);
            tvJudulArtikel = itemView.findViewById(R.id.tv_item_name);
            tvIsiArtikel = itemView.findViewById(R.id.tv_item_detail);
        }
    }
}
