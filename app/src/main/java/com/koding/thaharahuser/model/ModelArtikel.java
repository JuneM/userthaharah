package com.koding.thaharahuser.model;

import com.google.firebase.database.IgnoreExtraProperties;

import java.io.Serializable;

@IgnoreExtraProperties
public class ModelArtikel implements Serializable {
    String judulArtikel;
    String isiArtikel;
    String key;
    String uid;
    String username;

    public  ModelArtikel(){

    }

    public ModelArtikel(String judulArtikel, String isiArtikel, String uid, String username) {
        this.judulArtikel = judulArtikel;
        this.isiArtikel = isiArtikel;
        this.key = key;
        this.uid = uid;
        this.username = username;
    }

    public String getJudulArtikel() {
        return judulArtikel;
    }

    public void setJudulArtikel(String judulArtikel) {
        this.judulArtikel = judulArtikel;
    }

    public String getIsiArtikel() {
        return isiArtikel;
    }

    public void setIsiArtikel(String isiArtikel) {
        this.isiArtikel = isiArtikel;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
