package com.koding.thaharahuser.model;

import java.io.Serializable;

public class ModelJawaban implements Serializable {
    public String bila_benar;
    public String bila_salah;
    public String kondisi;
    public String start;
    public String stop;
    public String key;

    public String getKey() { return key; }

    public void setKey(String key) {
        this.key = key;
    }

    public String getBila_benar() {
        return bila_benar;
    }

    public void setBila_benar(String bila_benar) {
        this.bila_benar = bila_benar;
    }

    public String getBila_salah() {
        return bila_salah;
    }

    public void setBila_salah(String bila_salah) {
        this.bila_salah = bila_salah;
    }

    public String getKondisi() {
        return kondisi;
    }

    public void setKondisi(String kondisi) {
        this.kondisi = kondisi;
    }

    public String getStart() {
        return start;
    }

    public void setStart(String start) {
        this.start = start;
    }

    public String getStop() {
        return stop;
    }

    public void setStop(String stop) {
        this.stop = stop;
    }

}
