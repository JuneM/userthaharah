package com.koding.thaharahuser.model;


import com.google.firebase.database.IgnoreExtraProperties;
import java.io.Serializable;

@IgnoreExtraProperties
public class ModelPertanyaan implements Serializable {
    public String bila_benar;
    public String bila_salah;
    public String key;
    public String kondisi;
    public String start;
    public String stop;
    public String uid;
    public String username;

    public ModelPertanyaan(){

    }

    public ModelPertanyaan(String bila_benar, String bila_salah, String kondisi, String start, String stop, String uid, String username) {
        this.bila_benar = bila_benar;
        this.bila_salah = bila_salah;
        this.key = key;
        this.kondisi = kondisi;
        this.start = start;
        this.stop = stop;
        this.uid = uid;
        this.username = username;
    }

    public String getBila_benar() {
        return bila_benar;
    }

    public void setBila_benar(String bila_benar) {
        this.bila_benar = bila_benar;
    }

    public String getBila_salah() {
        return bila_salah;
    }

    public void setBila_salah(String bila_salah) {
        this.bila_salah = bila_salah;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getKondisi() {
        return kondisi;
    }

    public void setKondisi(String kondisi) {
        this.kondisi = kondisi;
    }

    public String getStart() {
        return start;
    }

    public void setStart(String start) {
        this.start = start;
    }

    public String getStop() {
        return stop;
    }

    public void setStop(String stop) {
        this.stop = stop;
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
