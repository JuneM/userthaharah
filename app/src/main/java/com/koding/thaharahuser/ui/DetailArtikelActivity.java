package com.koding.thaharahuser.ui;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.database.DatabaseReference;
import com.koding.thaharahuser.R;
import com.koding.thaharahuser.adapter.ListArtikelAdapter;
import com.koding.thaharahuser.model.ModelArtikel;

public class DetailArtikelActivity extends AppCompatActivity {

    public static final String EXTRA_DATA = "extra_data";
    private ModelArtikel modelArtikel;
    private TextView tvJudulArtikel, tvIsiArtikel;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_artikel);
        modelArtikel = (ModelArtikel) getIntent().getSerializableExtra(EXTRA_DATA);
        setUpFindView();
        setFetchData();
    }
    private void setFetchData() {
        tvJudulArtikel.setText(modelArtikel.getJudulArtikel());
        tvIsiArtikel.setText(modelArtikel.getIsiArtikel());
    }

    private void setUpFindView() {
        tvJudulArtikel = findViewById(R.id.judulArtikel);
        tvIsiArtikel = findViewById(R.id.isiArtikel);

    }

    @Override
    public void onBackPressed() {
        Intent intentBack = new Intent(getApplicationContext(), ListArtikelActivity.class);
        startActivity(intentBack);
        finish();
    }
}
