package com.koding.thaharahuser.ui;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

import com.koding.thaharahuser.R;

public class FormInputActivity extends AppCompatActivity {
    private ImageView ivNext;
    private EditText etNama;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form_input);
        setUpFindView();
        setUi();
        toolListener();
    }

    private void setUi() {
        SharedPreferences formBiodata = getApplicationContext().getSharedPreferences("FormBiodata", Context.MODE_PRIVATE);
        String hasNama = formBiodata.getString("nama", null);
        if (hasNama != null){
            etNama.setText(hasNama);
        }
    }

    private void toolListener() {
        ivNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String valNama = etNama.getText().toString();
                if (!validateForm(valNama)){
                    return;
                }
                SharedPreferences formBiodata = getApplicationContext().getSharedPreferences("FormBiodata", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor =formBiodata.edit();
                editor.putString("nama",valNama);
                editor.apply();
                Intent intent = new Intent(getApplicationContext(), KonsultasiActivity.class);
                startActivity(intent);
                finish();
            }
        });

    }

    private boolean validateForm(String valNama) {
        boolean valid = true;

        if (TextUtils.isEmpty(valNama)) {
            etNama.setError("Required.");
            valid = false;
        } else {
            etNama.setError(null);
        }

        return valid;
    }

    private void setUpFindView() {
        etNama = findViewById(R.id.et_nama);
        ivNext = findViewById(R.id.btn_next);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
        startActivity(intent);
    }
}
