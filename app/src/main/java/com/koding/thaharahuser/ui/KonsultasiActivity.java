package com.koding.thaharahuser.ui;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;
import com.koding.thaharahuser.BaseActivity;
import com.koding.thaharahuser.R;
import com.koding.thaharahuser.model.ModelJawaban;
import com.koding.thaharahuser.model.ModelPertanyaan;

import java.util.ArrayList;

public class KonsultasiActivity extends BaseActivity {

    private ImageView ivNext, ivBack;
    private TextView tvPertanyaan;
    private RadioGroup rdGrJawaban;
    private FirebaseDatabase mDatabase;
    private ArrayList<ModelPertanyaan> mListPertanyaan;
    private ArrayList<ModelJawaban> mListJawaban;
    private Button btnCetakHasil;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        showProgressDialog();
        setContentView(R.layout.activity_konsultasi);
        mDatabase = FirebaseDatabase.getInstance();
        DatabaseReference myRef = mDatabase.getReference("identifikasi_kondisi");
        setUpFindView();
        fecthDataFirebase(myRef);
    }

    private void setUpFindView() {
        ivBack = findViewById(R.id.btn_back);
        ivNext = findViewById(R.id.btn_next);
        rdGrJawaban = findViewById(R.id.rd_gr_jawaban);
        tvPertanyaan = findViewById(R.id.pertanyaan);
        btnCetakHasil = findViewById(R.id.btn_cetak_hasil);
    }

    private void fecthDataFirebase(DatabaseReference myRef) {
        myRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                mListPertanyaan = new ArrayList<>();
                for (DataSnapshot noteDataSnapshoot : dataSnapshot.getChildren()) {
                    ModelPertanyaan modelPertanyaan = noteDataSnapshoot.getValue(ModelPertanyaan.class);
                    modelPertanyaan.setKey(noteDataSnapshoot.getKey());
                    mListPertanyaan.add(modelPertanyaan);
                }
                mListJawaban = new ArrayList<>();
                startKonsultasi(mListJawaban, mListPertanyaan);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    private void startKonsultasi(ArrayList<ModelJawaban> mListJawaban, ArrayList<ModelPertanyaan> mListPertanyaan) {
        String bila_benar = "";
        String bila_salah = "";
        for (int i = 0; i < mListPertanyaan.size(); i++) {
            if (mListPertanyaan.get(i).start.equals("Ya")) {
                tvPertanyaan.setText(mListPertanyaan.get(i).getKondisi());
                bila_benar = mListPertanyaan.get(i).getBila_benar();
                bila_salah = mListPertanyaan.get(i).getBila_salah();
                ModelJawaban modelJawaban = new ModelJawaban();
                modelJawaban.setBila_benar(mListPertanyaan.get(i).getBila_benar());
                modelJawaban.setBila_salah(mListPertanyaan.get(i).getBila_salah());
                modelJawaban.setKondisi(mListPertanyaan.get(i).getKondisi());
                modelJawaban.setStart(mListPertanyaan.get(i).getStart());
                modelJawaban.setStop(mListPertanyaan.get(i).getStop());
                mListJawaban.add(modelJawaban);
                Gson gson = new Gson();
                String media = gson.toJson(mListJawaban);
                Log.d("clannad",media);
            }
        }
        hideProgressDialog();
        final String valBila_salah = bila_salah;
        final String valBila_benar = bila_benar;
        btnListenerNext(valBila_benar, valBila_salah, mListPertanyaan);
        btnListenerBack(mListJawaban, mListPertanyaan);
    }

    private void btnListenerBack(final ArrayList<ModelJawaban> mListJawaban, final ArrayList<ModelPertanyaan> mListPertanyaan) {
        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d("nana", "back clicked");
                if (mListJawaban.size() == 1) {
                    Intent intent = new Intent(getApplicationContext(), FormInputActivity.class);
                    startActivity(intent);
                    finish();
                } else {
                    rdGrJawaban.setVisibility(View.VISIBLE);
                    rdGrJawaban.clearCheck();
                    int indexPrev = mListJawaban.size() - 2;
                    int indexNow = mListJawaban.size() - 1;
                    Log.d("nana", String.valueOf(mListJawaban.size()) + String.valueOf(indexPrev) + String.valueOf(indexNow));
                    Gson gson = new Gson();
                    String media = gson.toJson(mListJawaban);
                    Log.d("clannad",media);
                    mListJawaban.remove(indexNow);
                    tvPertanyaan.setText(mListJawaban.get(indexPrev).getKondisi());
                    String benar = mListJawaban.get(indexPrev).getBila_benar();
                    String salah = mListJawaban.get(indexPrev).getBila_salah();

                }
            }
        });
    }

    @Override
    public void onBackPressed() {

    }

    private void btnListenerNext(final String valBila_benar, final String valBila_salah, final ArrayList<ModelPertanyaan> mListPertanyaan) {
        ivNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d("nana", "next clicked");
                int id = rdGrJawaban.getCheckedRadioButtonId();
                switch (id) {
                    case R.id.rd_yes:

                        int index = getIndex(valBila_benar,mListPertanyaan);
                        tvPertanyaan.setText(mListPertanyaan.get(index).getKondisi());
                        rdGrJawaban.clearCheck();
                        if (mListPertanyaan.get(index).stop.equals("Ya")){
                            final ModelJawaban modelJawaban = new ModelJawaban();
                            modelJawaban.setBila_benar(mListPertanyaan.get(index).getBila_benar());
                            modelJawaban.setBila_salah(mListPertanyaan.get(index).getBila_salah());
                            modelJawaban.setKondisi(mListPertanyaan.get(index).getKondisi());
                            modelJawaban.setStart(mListPertanyaan.get(index).getStart());
                            modelJawaban.setStop(mListPertanyaan.get(index).getStop());
                            modelJawaban.setKey(mListPertanyaan.get(index).getKey());
                            if (mListJawaban.get(mListJawaban.size()-1).getKondisi().equals(tvPertanyaan.getText())){
//                                Intent intentSolusi = new Intent(getApplicationContext(), SolusiActivity.class);
//                                intentSolusi.putExtra(SolusiActivity.EXTRA_DATA, modelJawaban);
//                                startActivity(intentSolusi);
//                                finish();
                            }else{
                                rdGrJawaban.check(R.id.rd_yes);
                                rdGrJawaban.setVisibility(View.GONE);
                                btnCetakHasil.setVisibility(View.VISIBLE);
                                ivNext.setVisibility(View.GONE);
                                ivBack.setVisibility(View.GONE);
                                btnCetakHasil.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        Intent intentSolusi = new Intent(getApplicationContext(), SolusiActivity.class);
                                        intentSolusi.putExtra(SolusiActivity.EXTRA_DATA, modelJawaban);
                                        startActivity(intentSolusi);
                                        finish();
                                    }
                                });
                                mListJawaban.add(modelJawaban);
                            }

                        }else{
                            String benar = mListPertanyaan.get(index).getBila_benar();
                            String salah = mListPertanyaan.get(index).getBila_salah();

                            ModelJawaban modelJawaban = new ModelJawaban();
                            modelJawaban.setBila_benar(mListPertanyaan.get(index).getBila_benar());
                            modelJawaban.setBila_salah(mListPertanyaan.get(index).getBila_salah());
                            modelJawaban.setKondisi(mListPertanyaan.get(index).getKondisi());
                            modelJawaban.setStart(mListPertanyaan.get(index).getStart());
                            modelJawaban.setStop(mListPertanyaan.get(index).getStop());
                            modelJawaban.setKey(mListPertanyaan.get(index).getKey());
                            mListJawaban.add(modelJawaban);
                            btnListenerNext(benar, salah, mListPertanyaan);
                            Gson gson = new Gson();
                            String media = gson.toJson(mListJawaban);
                            Log.d("clannad",media);
                        }
                        break;
                    case R.id.rd_no:
                        Gson gson2 = new Gson();
                        String media2 = gson2.toJson(mListJawaban);
                        Log.d("clannad",media2);
                        int indexNo = getIndex(valBila_salah,mListPertanyaan);
                        tvPertanyaan.setText(mListPertanyaan.get(indexNo).getKondisi());
                        rdGrJawaban.clearCheck();
                        if (mListPertanyaan.get(indexNo).stop.equals("Ya")){
                            final ModelJawaban modelJawaban = new ModelJawaban();
                            modelJawaban.setBila_benar(mListPertanyaan.get(indexNo).getBila_benar());
                            modelJawaban.setBila_salah(mListPertanyaan.get(indexNo).getBila_salah());
                            modelJawaban.setKondisi(mListPertanyaan.get(indexNo).getKondisi());
                            modelJawaban.setStart(mListPertanyaan.get(indexNo).getStart());
                            modelJawaban.setStop(mListPertanyaan.get(indexNo).getStop());
                            modelJawaban.setKey(mListPertanyaan.get(indexNo).getKey());
                            if (mListJawaban.get(mListJawaban.size()-1).getKondisi().equals(tvPertanyaan.getText())){
//                                Intent intentSolusi = new Intent(getApplicationContext(), SolusiActivity.class);
//                                intentSolusi.putExtra(SolusiActivity.EXTRA_DATA, modelJawaban);
//                                startActivity(intentSolusi);
//                                finish();
                            }else{
                                rdGrJawaban.check(R.id.rd_no);
                                rdGrJawaban.setVisibility(View.GONE);
                                btnCetakHasil.setVisibility(View.VISIBLE);
                                ivNext.setVisibility(View.GONE);
                                ivBack.setVisibility(View.GONE);
                                btnCetakHasil.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        Intent intentSolusi = new Intent(getApplicationContext(), SolusiActivity.class);
                                        intentSolusi.putExtra(SolusiActivity.EXTRA_DATA, modelJawaban);
                                        startActivity(intentSolusi);
                                        finish();
                                    }
                                });
                                mListJawaban.add(modelJawaban);
                            }
                        }else{
                            String benar = mListPertanyaan.get(indexNo).getBila_benar();
                            String salah = mListPertanyaan.get(indexNo).getBila_salah();

                            ModelJawaban modelJawaban = new ModelJawaban();
                            modelJawaban.setBila_benar(mListPertanyaan.get(indexNo).getBila_benar());
                            modelJawaban.setBila_salah(mListPertanyaan.get(indexNo).getBila_salah());
                            modelJawaban.setKondisi(mListPertanyaan.get(indexNo).getKondisi());
                            modelJawaban.setStart(mListPertanyaan.get(indexNo).getStart());
                            modelJawaban.setStop(mListPertanyaan.get(indexNo).getStop());
                            modelJawaban.setKey(mListPertanyaan.get(indexNo).getKey());
                            mListJawaban.add(modelJawaban);
                            btnListenerNext(benar, salah, mListPertanyaan);
                        }
                        break;
                }
            }
        });
    }

    private int getIndex(String valBila_benar, ArrayList<ModelPertanyaan> mListPertanyaan) {
        for (int i = 0; i < mListPertanyaan.size(); i++) {
            if (valBila_benar.equals(mListPertanyaan.get(i).key)) {
                return i;
            }
        }
        return 0;
    }
}
