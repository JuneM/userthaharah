package com.koding.thaharahuser.ui;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.koding.thaharahuser.R;
import com.koding.thaharahuser.adapter.ListArtikelAdapter;
import com.koding.thaharahuser.model.ModelArtikel;
import com.koding.thaharahuser.model.ModelPertanyaan;

import java.util.ArrayList;

public class ListArtikelActivity extends AppCompatActivity {

    private FirebaseDatabase mDatabase;
    private RecyclerView rvArtikel;
    private ArrayList<ModelArtikel> listArtikel = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_artikel);

        rvArtikel = findViewById(R.id.rv_artikel);
        rvArtikel.setHasFixedSize(true);

//        listArtikel.addAll(HeroesData.getListData());
        getDataFireBase();

    }

    private void getDataFireBase() {
        mDatabase = FirebaseDatabase.getInstance();
        DatabaseReference myRef = mDatabase.getReference("artikel");
        myRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                listArtikel = new ArrayList<>();
                for (DataSnapshot noteDataSnapshoot : dataSnapshot.getChildren()) {
                    ModelArtikel modelArtikel = noteDataSnapshoot.getValue(ModelArtikel.class);
                    modelArtikel.setKey(noteDataSnapshoot.getKey());
                    listArtikel.add(modelArtikel);
                }
                showRecyclerList();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
    private void showRecyclerList() {
        rvArtikel.setLayoutManager(new LinearLayoutManager(this));
        ListArtikelAdapter listArtikelAdapter = new ListArtikelAdapter(ListArtikelActivity.this , listArtikel);
        rvArtikel.setAdapter(listArtikelAdapter);
    }

    @Override
    public void onBackPressed() {
        Intent intentBack = new Intent(getApplicationContext(), MainActivity.class);
        startActivity(intentBack);
        finish();
    }
}
