package com.koding.thaharahuser.ui;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.annotation.Nullable;

import com.koding.thaharahuser.BaseActivity;
import com.koding.thaharahuser.R;

public class MainActivity extends BaseActivity {
    private Button btnKonsultasi, btnArtikel;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setUpFindView();
        toolListener();
    }

    private void toolListener() {
        btnKonsultasi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), FormInputActivity.class);
                startActivity(intent);
                finish();
            }
        });
        btnArtikel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), ListArtikelActivity.class);
                startActivity(intent);
                finish();
            }
        });
    }

    private void setUpFindView() {
        btnKonsultasi = findViewById(R.id.btn_konsultasai);
        btnArtikel = findViewById(R.id.btn_artikel);
    }
}
