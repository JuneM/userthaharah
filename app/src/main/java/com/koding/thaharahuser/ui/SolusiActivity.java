package com.koding.thaharahuser.ui;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;
import com.koding.thaharahuser.R;
import com.koding.thaharahuser.model.ModelCaraAtauBenda;
import com.koding.thaharahuser.model.ModelJawaban;
import com.koding.thaharahuser.model.ModelPertanyaan;
import com.koding.thaharahuser.model.ModelRule;

import java.util.ArrayList;

public class SolusiActivity extends AppCompatActivity {

    public static final String EXTRA_DATA = "extra_data";
    private ModelJawaban modelJawaban;
    private TextView tvNama, tvHasil, tvHal, tvKet, tvMazhab;
    private FirebaseDatabase mDatabase;
    private ArrayList<ModelRule> mListRule;
    private ArrayList<ModelCaraAtauBenda> mListCaraAtauBenda;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_solusi);
        mDatabase = FirebaseDatabase.getInstance();
        DatabaseReference myRef = mDatabase.getReference("aturan");
        modelJawaban = (ModelJawaban) getIntent().getSerializableExtra(EXTRA_DATA);
        setFindView();
        fecthData(modelJawaban, myRef);

    }

    private void fecthData(ModelJawaban modelJawaban, DatabaseReference myRef) {
        final String keyKondisi = modelJawaban.getKey();
        myRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                mListRule = new ArrayList<>();
                for (DataSnapshot noteDataSnapshoot : dataSnapshot.getChildren()) {
                    ModelRule modelRule = noteDataSnapshoot.getValue(ModelRule.class);
                    mListRule.add(modelRule);
                }
                String keySolusi = "";
                if (mListRule.size() != 0){
                    for (int i=0; i<mListRule.size(); i++){
                        if (keyKondisi.equals(mListRule.get(i).getKey_kondisi())){
                            keySolusi = mListRule.get(i).getKey_solusi();
                        }
                    }
                }else{
                    Toast.makeText(SolusiActivity.this, "data rule tidak ada", Toast.LENGTH_SHORT).show();
                }
                Log.d("anohhh",keySolusi);
                DatabaseReference myRefSolusi = mDatabase.getReference("cara_benda_bersuci");
                final String finalKeySolusi = keySolusi;
                myRefSolusi.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                        mListCaraAtauBenda = new ArrayList<>();
                        for (DataSnapshot noteDataSnapshoot : dataSnapshot.getChildren()) {
                            ModelCaraAtauBenda modelCaraAtauBenda = noteDataSnapshoot.getValue(ModelCaraAtauBenda.class);
                            modelCaraAtauBenda.setKey(noteDataSnapshoot.getKey());
                            mListCaraAtauBenda.add(modelCaraAtauBenda);
                        }
                        Gson gson = new Gson();
                        String media = gson.toJson(mListCaraAtauBenda);
                        Log.d("anoh",media);

                        if (mListCaraAtauBenda.size() != 0){
                            String ket = "";
                            String mazhab = "";
                            String solusi = "";
                            String sumber = "";
                            for (int i=0; i<mListCaraAtauBenda.size(); i++){
                                if (finalKeySolusi.equals(mListCaraAtauBenda.get(i).getKey())){
                                    ket = mListCaraAtauBenda.get(i).getKeterangan();
                                    mazhab = mListCaraAtauBenda.get(i).getMazhab();
                                    solusi = mListCaraAtauBenda.get(i).getSolusi();
                                    sumber = mListCaraAtauBenda.get(i).getSumber();
                                }else{
                                    Log.d("anohh", "tidak sama");
                                }
                            }
                            SharedPreferences formBiodata = getApplicationContext().getSharedPreferences("FormBiodata", Context.MODE_PRIVATE);
                            String hasNama = formBiodata.getString("nama", null);
                            Log.d("ano",hasNama + " "+ ket+" "+solusi+" "+sumber);
                            tvNama.setText(hasNama);
                            tvKet.setText(ket);
                            tvMazhab.setText(mazhab);
                            tvHasil.setText(solusi);
                            tvHal.setText(sumber);
                            getApplication().getSharedPreferences("FormBiodata", 0).edit().clear().commit();
                        }else{
                            Toast.makeText(SolusiActivity.this, "Data Kosong", Toast.LENGTH_SHORT).show();
                            getApplication().getSharedPreferences("FormBiodata", 0).edit().clear().commit();
                        }

                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                });
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }

    private void setFindView() {
        tvNama = findViewById(R.id.tv_nama);
        tvHasil = findViewById(R.id.tv_hasil);
        tvHal = findViewById(R.id.tv_hal);
        tvKet = findViewById(R.id.tv_ket);
        tvMazhab = findViewById(R.id.tv_mazhab);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intenHome = new Intent(getApplicationContext(), MainActivity.class);
        startActivity(intenHome);
    }
}
